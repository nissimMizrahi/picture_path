use std::collections::LinkedList;

mod point;
pub use point::Point;

mod maze_node;
pub use maze_node::Node;

macro_rules! MAZE {
    
    (no_entry) => (255);
    (invalid_point_score) => (-1.0);
}

fn get_point_g(src: &Point, point: &Point, track: &LinkedList<Point>) -> f64
{
    let mut iter = track.iter();
    let last_point: &Point = match iter.next()
    {
        None => src,
        Some(x) => x,
    };

    let last_distance = track.len();
    let dis_to_last = point.distance(&last_point);

    if dis_to_last < 2.0 {
        return (last_distance + 1) as f64;
    }else{
        return MAZE!(invalid_point_score);
    };
}

fn get_total_score(src: &Point, dst: &Point, point: &Point, track: &LinkedList<Point>, field: &Vec<Vec<Node>>) -> f64
{
    let g = get_point_g(src, point, &track);
    let h = point.distance(dst);
    let f = field[point.y as usize][point.x as usize].get_value() as f64 / 255.0;

    if field[point.y as usize][point.x as usize].get_value() == MAZE!(no_entry) || g == MAZE!(invalid_point_score)
    {
        return MAZE!(invalid_point_score);
    }
    else
    {
        return g + h + f * 4.0;
    }
}

pub fn get_value(src: &Point, 
                dst: &Point, 
                track: &LinkedList<Point>, 
                blacklist: &LinkedList<Point>, 
                field: &Vec<Vec<Node>>)
                -> Point
{

    let (width, height) = (field[0].len(), field.len());

    let mut scores = Vec::new();

    let mut iter = track.iter();
    let last_point: &Point = match iter.next()
    {
        None => src,
        Some(x) => x,
    };


    let rigth = if last_point.x >= (width - 1) as f64 {0.0} else {1.0};
    let up = if last_point.y <= 0.0 {0.0} else {1.0};
    let left = if last_point.x <= 0.0 {0.0} else {1.0};
    let down = if last_point.y >= (height - 1) as f64 {0.0} else {1.0};


    scores.push(Point::new(last_point.x + rigth, last_point.y));
    scores.push(Point::new(last_point.x - left, last_point.y));

    scores.push(Point::new(last_point.x, last_point.y + down));
    scores.push(Point::new(last_point.x, last_point.y - up));

    let mut ret_point = last_point;

    for point in scores.iter()
    {
        let point_score = get_total_score(src, dst, point, &track, field);

        if point_score == MAZE!(invalid_point_score) || point == last_point || blacklist.contains(point)
        {
            continue;
        }
        else if point_score < get_total_score(src, dst, ret_point, &track, field)
        {
            ret_point = point;
        }
    }
    return ret_point.clone();
}

pub fn get_next_point(dst: &Point,
                    track: &mut LinkedList<Point>,
                    blacklist: &mut LinkedList<Point>, 
                    maze: &Vec<Vec<Node>>)
                    -> Result<Point, String>
{
    if track.is_empty(){
        return Err("no starting point".to_string());
    }
    
    if *track.front().unwrap() != *dst
    {
        let next_point = get_value(track.front().unwrap(), dst, &track, &blacklist, maze);
        
        if !track.is_empty()
        {
            if next_point != *track.front().unwrap()
            {
                track.push_front(next_point.clone());
            }
            else
            {
                blacklist.push_front(track.pop_front().unwrap());
            }
        }
        return Ok(next_point);
    }
    else
    {
        return Ok(dst.clone());
    }
    
}

pub fn get_path(src: &Point, dst: &Point, maze: &Vec<Vec<Node>>) -> Result<(LinkedList<Point>, LinkedList<Point>), String> 
{
    let mut track = LinkedList::new();
    let mut blacklist = LinkedList::new();

    track.push_front(src.clone());

    let mut next_point = src.clone();
    while next_point != *dst
    {
        next_point = match get_next_point(&dst, &mut track, &mut blacklist, maze)
        {
            Ok(p) => p,
            Err(e) => return Err("cant complete maze".to_string()),
        };
    }
        
    return Ok((track, blacklist));
}