#[derive(Clone)]
pub struct Node
{
    stepped: bool,
    x_stepped: bool,
    height: u8
}

impl Node
{
    pub fn new(height: u8) -> Node
    {
        return Node{stepped: false, x_stepped:false, height: height};
    }

    pub fn get_value(&self) -> u8
    {
        return self.height;
    }
    pub fn set_value(&mut self, height: u8)
    {
        self.height = height;
    }
    pub fn is_stepped(&self) -> bool
    {
        return self.stepped;
    }
    pub fn step(&mut self)
    {
        self.stepped = true;
        self.x_stepped = false;
    }
    pub fn backtrack(&mut self)
    {
        self.stepped = false;
        self.x_stepped = true;
    }
    pub fn was_stepped(&self) -> bool
    {
        return self.x_stepped;
    }
}