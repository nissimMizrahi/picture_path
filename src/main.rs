extern crate image;
use image::{
    GenericImageView, 
    DynamicImage, 
    ImageBuffer,
    Rgba
    };

#[macro_use]
mod a_star;
use a_star::{Point, Node};

fn get_vecs(pic: &DynamicImage) -> Vec<Vec<Node>>
{
    let (width, height) = pic.dimensions();
    let mut buffer = vec![vec![Node::new(0); width as usize]; height as usize];

    for (x, y, pixel) in pic.pixels() {

        let value = if pixel[0] > 50 {255} else {0}; //not used

        buffer[y as usize][x as usize].set_value(pixel[0]);
    }
    return buffer;
}

fn main() {
    // Use the open function to load an image from a Path.
    // ```open``` returns a `DynamicImage` on success.
    let img = image::open("map-terrain.png").unwrap();
    let (width, height) = img.dimensions();

    let kernel: [f32; 9] = [-1.0, -1.0, -1.0,
                            -1.0, 8.0, -1.0,
                            -1.0, -1.0, -1.0];

    let grayed = img.grayscale();
    let mut filtered = grayed.filter3x3(&kernel);

    let mut buff = get_vecs(&filtered);
    let (start, finish) = (Point::new(200.0, 200.0), Point::new((width - 200) as f64, (height - 200) as f64));

    let (track, blacklist) = match a_star::get_path(&start, &finish, &buff)
    {
        Ok((t, b)) => (t, b),
        Err(e) => {
            println!("{}", e);
            return;
        },
    };

    let mut filtered_rgba = filtered.to_rgba();
    for point in track.iter()
    {
        filtered_rgba.put_pixel(point.x as u32, point.y as u32,  Rgba([0, 255, 0, 255]));
    }
    for point in blacklist.iter()
    {
        filtered_rgba.put_pixel(point.x as u32, point.y as u32, Rgba([255, 0, 0, 255]));
    }
    
    filtered_rgba.save("test.png").unwrap();
}